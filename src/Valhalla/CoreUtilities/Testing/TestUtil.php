<?php

/* 
 * Test Utility
 * Useful functions for unit and integration testing.
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\CoreUtilities\Testing;

class TestUtil {
    
    /**
     * Make a class's private or protected method into a temporarily public one.
     * 
     * @param string $className
     * @param string $methodName
     * @return \ReflectionMethod
     */
    public static function publicizeMethod($className, $methodName){
        $class = new \ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        
        return $method;
    }
    
}