## 3.2.0

*Bug Fixes:*

  - Renamed Data/String to Data/Strings to resolve naming conflict with new PHP7 type


## 3.1.7

*Bug Fixes:*

  - Fixed error if data in memcached exists but was not created with this memcached class (and so format is different)

## 3.1.6

*Features:*

  - Adding the ability to BasicRequest to add extra headers to the request (they could not be merged before because it was a string, now it is an array).

*Bug Fixes:*

  - Making it so there is no exception in Memcached when there was no cached data and new data could not be retrieved.
  - Fixed bug where last SEO page does not show up for pagination
  - Preventing error in the catch block of memcached function

## 3.1.5

*Bug Fixes:*

  - Fixed PHPDoc in Collection and a number of other classes
  - Improved error reporting in cURL

*Other:*

  - Adding more documentation

## 3.1.4

*Features:*

  - Adding FileSystem utility class
  - Adding a bunch of functions to Collection

*Other:*

  - Adding markdown-style documentation

## 3.1.3

*Features:*

  - Adding TestUtil class
  - cURL now throws an exception on failure

*Bug Fixes:*

  - Fixed naming problem with a method in JsonConfig

*Other:*

  - Adding more unit tests

## 3.1.2

*Features:*

  - Adding getFromArray function for collections

*Bug Fixes:*

  - Fixing PHPDoc in a few places

*Other:*

  - Adding more unit tests

## 3.1.1

*Features:*

  - Adding vsprintfNamed function for strings

## 3.1.0

*Features:*

  - Making HttpSession a singleton now.
  - Renaming ArrayUtils to Collection.
  - Adding Data\String class and Data\Url class.

*Other:*

  - Adding some unit tests


## 3.0.0

*Features:*

  - Added the following classes: BasicRequest, RequestInterface, ArrayUtil
  - The SqlPdo class has been renamed to Pdo and moved to a new subdirectory
  - The cURL class has been moved to a new subdirectory and now implements an interface
