Security Utilities
==================
The Valhalla Core Utils Lib has a variety of useful security tools to make it easier to secure your applications. Currently we have a module for preventing cross-site request forgery vulnerabilities.

Current Modules:
* CSRF Token