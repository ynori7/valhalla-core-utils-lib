<?php
/*
 * A set of useful  functions.
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\CoreUtilities\Data;

class Strings { 
    
    const ELLIPSIS = '...';
    const UTF8 = 'UTF-8';

    /**
     * Returns true if $haystack begins with $needle.
     * 
     * @param  $haystack
     * @param  $needle
     * @param  $encoding Optional. The character encoding for the string.
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public static function startsWith($haystack, $needle, $encoding = self::UTF8) {
        if (empty($needle)) {
            throw new \InvalidArgumentException("Needle must not be an empty .");
        }
        $length = mb_strlen($needle, $encoding); 
        return (mb_substr($haystack, 0, $length, $encoding) === $needle);
    }

    /**
     * Returns true if $haystack ends with $needle.
     * 
     * @param  $haystack
     * @param  $needle
     * @param  $encoding Optional. The character encoding for the string.
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public static function endsWith($haystack, $needle, $encoding = self::UTF8) {
        if (empty($needle)) {
            throw new \InvalidArgumentException("Needle must not be an empty .");
        }
        $length = mb_strlen($needle, $encoding);
        return (mb_substr($haystack, -$length, $length, $encoding) === $needle);
    }
    
    /**
     * Truncates $text to $maxLength characters if the text is longer than that
     * and if so, adds an ellipsis to the end.
     * 
     * @param  $text
     * @param int $maxLength
     * @param  $ellipsis Optional. The suffix to stick to the end of the truncated string.
     * @param  $encoding Optional. The character encoding for the string.
     * @return 
     * @throws \InvalidArgumentException
     */
    public static function truncateWithEllipsis($text, $maxLength, $ellipsis = self::ELLIPSIS, 
            $encoding = self::UTF8) {
        
        if ($maxLength < strlen($ellipsis)) {
            throw new \InvalidArgumentException("Max Length must be greater than the length of the ellipsis.");
        }
        /* Needs to strip tags when it's possible for the  to contain HTML tags
         * because when displaying the  later it can lead to unclosed tags */
        $text = strip_tags($text); 
        $truncatedText = $text;

        if (mb_strlen($text, $encoding) > $maxLength) {
            $truncatedText = mb_substr($text, 0, $maxLength - strlen($ellipsis), $encoding) . $ellipsis;
        }
        
        return $truncatedText;
    }

    /**
     * Returns a  using a format string with named parameters.
     *
     * Example:
     * $foo = array('age' => 5, 'name' => 'john');
     * echo vsprintfNamed("{name} is {age}", $foo);
     *
     * @param  $format
     * @param array $args
     * @param boolean $failOnMissing Whether or not to throw an exception if a parameter is missing.
     * @return 
     * @throws \OutOfBoundsException
     */
    public static function vsprintfNamed($format, array $args, $failOnMissing = true) {
        $names = preg_match_all('/\{(.*?)\}/', $format, $matches, PREG_SET_ORDER);

        $values = array();
        foreach($matches as $match) {
            if(array_key_exists($match[1], $args)){
                $values[] = $args[$match[1]];
            } elseif (!$failOnMissing) {
                $values[] = '';
            } else {
                throw new \OutOfBoundsException(sprintf("Key %s was missing from the arguments.", $match[1]));
            }
        }

        $format = preg_replace('/\{(.*?)\}/', '%s', $format);

        return vsprintf($format, $values);
    }

}
