Data Utilities
==============
In this section you can find a collection of useful utilities for various data types. These each include many useful functions which build upon, add to, and extend existing PHP functions.

Included are:

* Collection Utilities which extend PHP's existing array functions and operators
* File System Utilities which add functions for file and directory operations
* String Utilities which extend PHP's existing string functions and operators
* URL Utilities which add useful functions for working with URLs