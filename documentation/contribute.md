Contributing
============
Here you can find everything you need to learn how to contribute to the valhalla-core-utils-lib project.

###Contents

- [How to Contribute](#how-to-contribute)
- [What Do I Need to Do?](#what-do-i-need-to-do)
- [How to Add Documentation](#how-to-add-docs)

The Valhalla Core Utilities Library is a free, open-source project intended to make clean development easy. To ensure that the library is always performing at its best and integrating the technologies that modern developers need, we strongly encourage contributing any ideas you have to the project, and even helping us write code, unit tests, and documentation.

<a id='how-to-contribute'></a>
How to Contribute
=================
The easiest way to contribute is by [forking](https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository) the [repository](https://bitbucket.org/ynori7/valhalla-core-utils-lib). After making your changes you can just make a pull request and when the changes are approved they'll be merged to the main repository.

If you prefer to use github over bitbucket, that's fine. Just create a new github project for your changes. Then clone the valhalla-core-utils-lib project, and then change the remote to point to your git repository:

    git remote set-url origin https://github.com/USERNAME/REPOSITORY.git

If you're allergic to git for some reason, you can also download the code from the links in the sidebar or on our bitbucket page, make your changes, and then email them to us.

###How to Contact Us
There are a number of ways to contact us. Here are a few of the best ways:

* Send us an email at [halls.of.valhalla.contact@gmail.com](mailto:halls.of.valhalla.contact@gmail.com). 
* Join our [IRC](http://halls-of-valhalla.org/beta/irc) channel and look for ynori7.
* Sent a private message on [Valhalla](http://halls-of-valhalla.org) to [ynori7](http://halls-of-valhalla.org/beta/user/ynori7).

<a id='what-do-i-need-to-do'></a>
What Do I Need to Do?
=====================
When writing code for the Core Utils Library, make sure you follow the [Symfony Coding Standards](http://symfony.com/doc/current/contributing/code/standards.html). We try to keep our code clean and uniform. 

Modules/Utilities belong under the `src` directory. Before adding any new modules, check first if it might make sense to add to or refactor existing modules.

When writing new code, make sure to add unit tests under the `tests` directory. Try to ensure as complete test coverage as possible.

When you add, remove, or change something, be sure to update the documentation in the `documentation` directory.

<a id='how-to-add-docs'></a>
How to Add Documentation
========================
The code for the documentation website can be found on [bitbucket](https://bitbucket.org/ynori7/coreutilslibdocumentation). You are free to check out this code and fork it as well, but it is not necessary in order to update the documentation. To update the documentation, you must simply modify the documents in the valhalla-core-utils-lib project in the `documentation` directory.

All documents for pages and documentation are text files containing markdown and end with the .md extension. The markdown will be automatically parsed and rendered on the documentation website.

Files directly in the `documentation` directory are pages and will be linked to in the main navigation. Examples include the main overview page and the change log.

In the `documentation/docs` directory you can find a directory structure containing directories named after module categories which contain an overview file and files for each module. The documentation website will automatically detect new files and directories and add them to the navigation.

When you make changes to the documentation and they are merged back to the master branch, we'll simply do a Composer update on the documentation site and the new changes will be applied.