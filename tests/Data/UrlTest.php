<?php

/* 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\Tests\Data;

use Valhalla\CoreUtilities\Data\Url;

/**
 * @covers Valhalla\CoreUtilities\Data\Url
 */
class UrlTest extends \PHPUnit_Framework_TestCase {
    
    public function testSlugify_EmptyString() {
        $actual = Url::slugify('');
        
        $this->assertEquals($actual, 'n-a');
    }
    
    public function testSlugify_OnlySpecialChars() {
        $actual = Url::slugify('~)@_-');
        
        $this->assertEquals($actual, 'n-a');
    }
    
    public function testSlugify_Normal() {
        $actual = Url::slugify('blah blah! I am a String :)');
        
        $this->assertEquals($actual, 'blah-blah-i-am-a-string');
    }
    
    public function testSlugify_Utf8Chars() {
        $currentLocale = setlocale(LC_ALL, 0);
        setlocale(LC_ALL, 'en_US');
        
        $actual = Url::slugify('ich weiß es nicht. ich bin ein gummibär');
        
        $this->assertEquals($actual, 'ich-weiss-es-nicht-ich-bin-ein-gummibar');
        
        setlocale(LC_ALL, $currentLocale);
    }
    
}