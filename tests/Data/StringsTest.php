<?php

/* 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\Tests\Data;

use Valhalla\CoreUtilities\Data\Strings;

/**
 * @covers Valhalla\CoreUtilities\Data\Strings
 */
class StringsTest extends \PHPUnit_Framework_TestCase {
    
    public function testStartsWith_Empty(){
        $actual = Strings::startsWith('', 'abc');
        
        $this->assertFalse($actual);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testStartsWith_EmptyNeedle(){
        Strings::startsWith('blah', '');
    }
    
    public function testStartsWith_Yes(){
        $actual = Strings::startsWith('abcde', 'abc');
        
        $this->assertTrue($actual);
    }
    
    public function testStartsWith_No(){
        $actual = Strings::startsWith('babcde', 'abc');
        
        $this->assertFalse($actual);
    }
    
    public function testStartsWith_Equal(){
        $actual = Strings::startsWith('abcde', 'abcde');
        
        $this->assertTrue($actual);
    }
    
    /********************************/
    
    public function testEndsWith_Empty(){
        $actual = Strings::endsWith('', 'abc');
        
        $this->assertFalse($actual);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testEndsWith_EmptyNeedle(){
        Strings::endsWith('blah', '');
    }
    
    public function testEndsWith_Yes(){
        $actual = Strings::endsWith('abcde', 'cde');
        
        $this->assertTrue($actual);
    }
    
    public function testEndsWith_No(){
        $actual = Strings::endsWith('babcde', 'abc');
        
        $this->assertFalse($actual);
    }
    
    public function testEndsWith_Equal(){
        $actual = Strings::endsWith('abcde', 'abcde');
        
        $this->assertTrue($actual);
    }
    
    /********************************/
    
    public function testTruncateWithEllipsis_Normal(){
        $actual = Strings::truncateWithEllipsis("abcdefghijklmnop", 15);
        
        $this->assertEquals('abcdefghijkl...', $actual);
    }
    
    public function testTruncateWithEllipsis_Empty(){
        $actual = Strings::truncateWithEllipsis("", 15);
        
        $this->assertEquals('', $actual);
    }
    
    public function testTruncateWithEllipsis_ShorterThanMax(){
        $actual = Strings::truncateWithEllipsis("abc", 15);
        
        $this->assertEquals('abc', $actual);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testTruncateWithEllipsis_EllipsisLongerThanMax(){
        $actual = Strings::truncateWithEllipsis("abcdefghijklmnop", 1);
    }
    
    /********************************/
    
    public $formatString1 = "abcd{blah}bcd{blorp}e";
    public $formatString2 = "abcd{blah}bcd{blorp}wer{blah}";
    
    public function testVsprintfNamed_NoParams(){
        $actual = Strings::vsprintfNamed("blah", array());
        
        $this->assertEquals('blah', $actual);
    }
    
    public function testVsprintfNamed_Normal(){
        $actual = Strings::vsprintfNamed($this->formatString1, 
                array('blah' => 'pie', 'blorp' => 'taco'));
        
        $this->assertEquals('abcdpiebcdtacoe', $actual);
    }
    
    public function testVsprintfNamed_DuplicateParamInString(){
        $actual = Strings::vsprintfNamed($this->formatString2, 
                array('blah' => 'pie', 'blorp' => 'taco'));
        
        $this->assertEquals('abcdpiebcdtacowerpie', $actual);
    }
    
    public function testVsprintfNamed_ExtraParams(){
        $actual = Strings::vsprintfNamed($this->formatString1, 
                array('blah' => 'pie', 'blorp' => 'taco', 'a' => 'b'));
        
        $this->assertEquals('abcdpiebcdtacoe', $actual);
    }
    
    /**
     * @expectedException \OutOfBoundsException
     */
    public function testVsprintfNamed_MissingParams_Fail(){
        $actual = Strings::vsprintfNamed($this->formatString1, 
                array('blah' => 'pie'), true);
    }
    
    public function testVsprintfNamed_MissingParams_DontFail(){
        $actual = Strings::vsprintfNamed($this->formatString1, 
                array('blah' => 'pie'), false);
        
        $this->assertEquals('abcdpiebcde', $actual);
    }
    
}
