Config
======
Here you can find classes for handing configuration, for example, opening, parsing, adding to, and merging configuration files. 

Currently we have the following config modules:

* JSON Config