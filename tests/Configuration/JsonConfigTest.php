<?php

/* 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\Tests\Configuration;

use Valhalla\CoreUtilities\Configuration\JsonConfig;
use Valhalla\CoreUtilities\Testing\TestUtil;

/**
 * @covers Valhalla\CoreUtilities\Configuration\JsonConfig
 */
class JsonConfigTest extends \PHPUnit_Framework_TestCase {
        
    public function testGetDataFromFile_Normal(){
        $method = TestUtil::publicizeMethod('Valhalla\CoreUtilities\Configuration\JsonConfig', 'getDataFromFile');
        
        $config = new JsonConfig();
        $actual = $method->invokeArgs($config, array(__DIR__ . '/../TestData/a.json'));
        
        $this->assertEquals(count($actual), 4);
    }
    
}