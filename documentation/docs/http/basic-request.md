Basic Request
=============
The Basic Request class uses built-in PHP functions to mimic the capabilities of cURL. This can be very useful when you are, for whatever reason, unable to install cURL, or unable to ensure that libcurl will be available on all servers where your code will run. This module contains functions to make the execution of GET, POST, PUT, and DELETE requests easy. All requests are configurable with various HTTP options.

###For Example:
    $req = new BasicRequest();
    $req->setOptions(array('http' => array('timeout' => 10)));
    $req->post('http://halls-of-valhalla.org', array('blah'=>234));

###Available Functions:
Here is an explanation of each of the available functions for BasicRequest.

---

**__construct: (The Constructor)**

*Parameters:*
None

*Return Values:*
None

Adds the default options to the array of options to be used for this request.

---

**setOptions:**

*Parameters:*
* array `$options`

*Return Values:*
None

Set extra context options to use for this instance. The new options are merged on top of the list of default options. See [HTTP Context Options](http://php.net/manual/en/context.http.php) for a list of available options and their possible values.

---

**getHeaderStringFromArray:**

*Parameters:*
* array `$headers` A list of HTTP headers

*Return Values:*
Returns an HTTP header string properly formatted with carriage returns and line breaks.

---

**get:**

*Parameters:*
* string `$url`

*Return Values:*
Returns a string containing the HTTP response.

Executes a GET request on the given URL.

---

**post:**

*Parameters:*
* string `$url`
* array `$payload` (Optional) The data to be posted. Defaults to empty array.

*Return Values:*
Returns a string containing the HTTP response.

Executes a POST request on the given URL with the given POST parameters.

---

**delete:**

*Parameters:*
* string `$url`

*Return Values:*
Returns a string containing the HTTP response.

Executes a DELETE request on the given URL.

---

**put:**

*Parameters:*
* string `$url`
* array `$payload` (Optional) The data to be posted. Defaults to empty array.

*Return Values:*
Returns a string containing the HTTP response.

Executes a PUT request on the given URL with the given PUT parameters.


---

**doRequest:**

*Parameters:*
* string `$url`
* array `$httpParams` (Optional) Options used for just this request (e.g. postfields). Will be merged with the options array which is based on default options and `setOptions()`. Defaults to empty array.

*Return Values:*
Returns a string containing the HTTP response. Throws an exception if the request failed.

Executes a request on the given URL. The functions `get()`, `post()`, `put()`, and `delete()` are actually wrappers for this function and the same functionality can be built manually using this function if needed. This function potentially allows more control since you are able to specify any additional HTTP  options you want for this request (if for some reason you do not want to set these options using `setOptions()`).
