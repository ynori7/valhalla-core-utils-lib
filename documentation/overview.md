Overview
========

This is a project intended to supply useful utility classes to ensure that you do not need to reinvent the wheel in each of your projects. 

Contents
-------
- [Why Should I Use This Library?](#why-to-install)
- [Installation](#installation)

 
<a id='why-to-install'></a>
Why Should I Use This Library?
------
The Valhalla Core Utils Library has many useful classes for many common tasks such as handling Memcached and MySQL connections, making cURL requests, handling sessions, and much more, all without the requirement to install any additional 3rd party libraries. 

Additionally this library has many helpful functions building upon and expanding the standard PHP libraries for things such as string manipulation, array operations, and even unit testing.

<a id='installation'></a>
Installation:
------
**In Composer Applications:**

The best way to use this project is to install it via Composer. You can find information about the most recent tags on [Packagist](https://packagist.org/packages/valhalla/valhalla-core-utils-lib). To install with Composer, add the following to your composer.json:

    "require": {
        "valhalla/valhalla-core-utils-lib": "dev-master"
    }

Now you can just do:

    use Valhalla\CoreUtilities\Http\cURL;
    
    $curl = new cURL();
    $curl->setOptions(array('CURLOPT_FAILONERROR' => false));
    print_r($curl->post('http://halls-of-valhalla.org', array('test'=>'blah')));
    
**In Non-Composer Applications:**

If you are not using composer with your project already, you can place the above composer.json code in your project and then execute the following commands from the root of this project:

    curl -sS https://getcomposer.org/installer | php
    php composer.phar install

Now you can include the composer autoloader in your files:

    include_once('valhalla-core-utils-lib/vendor/autoload.php');
    
