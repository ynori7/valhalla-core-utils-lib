Collection Utilities
====================
Valhalla's collection utilities extend PHP's existing array functions and operators to make advanced array manipulations, comparisons, and traversals effortless.

###Available Functions:
Here is a list of each of the available functions in Collection and how they can be used. Note that each of these functions is static and therefore can be called without instantiating any objects.

---

**merge:**

*Parameters:*
* array `$current`
* array `$new`
* boolean `$recursiveMerge` (Optional) Defaults to true

*Return Value:*
Returns an array containing values from `$current` and `$new`.

Accepts two arrays and an optional `$recursiveMerge` flag (defaults to `true`) to indicate whether or not to merge the arrays recursively. The function merges the two arrays. Note that the second array is merged on top of the first, so its values can overwrite values in the first array. If `$recurisveMerge` is true then the function merges the individual items of sub-arrays, otherwise it just replaces them entirely.

---

**arrayDiffRecursive:**

*Parameters:*
* array `$array1`
* array `$array2`

*Return Value:*
An array containing the entries from `$array1` that are not present in `$array2` or are different.

---

**getFromArray:**

*Parameters:*
* array `$array`
* string or int `$key`
* string `$default` (Optional) Defaults to empty string.
* int `$validateFlag` (Optional) Defaults to `FILTER_DEFAULT`

*Return Value:*
Returns the content in `$array` at the specified key.

Gets a value from the array at the specified key. If unavailable or invalid (based on the specified filter), return the optionally specified default value.

---

**getFromArrayPath:**

*Parameters:*
* array `$array`
* string `$path`
* string `$default` (Optional) Defaults to empty string.
* int `$validateFlag` (Optional) Defaults to `FILTER_DEFAULT`

*Return Value:*
Returns the data in `$array` at the specified path and validates it with the same logic as in `getFromArray()`.

*For example:*

    Collection::getFromArrayPath($x, 'data/config/use_cookies');
    #returns $x['data']['config']['use_cookies'];

---

**setArrayPath:**

*Parameters:*
* array `$array`
* string `$path`
* mixed `$value`
* boolean `$createFullPath` (Optional) Defaults to false

*Return Value:*
None

Sets a value in the array based on a specified path. If any of the indices in the middle of the path do not exist (or are not arrays) an exception will be thrown unless `$createFullPath` is true in which case each missing or invalid index will be created.

*For example:*

    $x = array('a' => array(1, 2, 3), 'b' => 'blah');
    # Note: $x is not reset in between each example
    setArrayPath($x, 'a/1', 5);
    # Result:
    # Array
    # (
    #     [a] => Array
    #         (
    #             [0] => 1
    #             [1] => 5
    #             [2] => 3
    #         )
    #     [b] => blah
    # )

    setArrayPath($x, 'd/4', 4);
    #Result exception

    setArrayPath($x, 'd/4', 4, true);
    # Result:
    # Array
    # (
    #     [a] => Array
    #         (
    #             [0] => 1
    #             [1] => 5
    #             [2] => 3
    #         )
    #     [b] => blah
    #     [d] => Array
    #         (
    #             [4] => 4
    #         )
    # )

    setArrayPath($x, 'b/b', 3, true);
    # Result:
    # Array
    # (
    #     [a] => Array
    #         (
    #             [0] => 1
    #             [1] => 5
    #             [2] => 3
    #         )
    #     [b] => Arrayls

    #         (
    #             [b] => 3
    #         )
    #     [d] => Array
    #         (
    #             [4] => 4
    #         )
    # )

---

**isLast:**

*Parameters:*
* array `$array`
* string or int `$key`

*Return Value:*
Returns `true` if the provided key is the last element of an array otherwise `false`.

---

**isFirst:**

*Parameters:*
* array `$array`
* string or int `$key`

*Return Value:*
Returns `true` if the provided key is the first element of an array otherwise `false`.