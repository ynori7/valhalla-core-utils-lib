PDO MySQL
=========
The Pdo class is a wraper class for the built-in \PDO class. Valhalla's Pdo class must be extended by another class which should define the database name and connection information. This ensures that you do not need to pass in the database configuration in every service where you use your class. Extending Valhalla's Pdo class also allows you to to make your DAO into a singleton if it is possible in your project (unfortunately the Pdo class cannot be a singleton by itself without imposing too many restrictions on the usability).

###Example Usage:
    use Valhalla\CoreUtilities\Database\SQL\Pdo;
    class DAO extends Pdo {
      protected $_database;
      protected $_host = "localhost";
      protected $_dbuser = "someone";
      protected $_dbpass = "somepassword";
    
      public function __construct($database='default'){
        //lookup your database config from your config file. For simplicity's sake the settings are hard-coded above
        $this->_database = $database;
        $this->init();
      }
    }

Next use your database class to query the database:

    $dao = new DAO('Users');
    var_dump($dao->executeQuery('SELECT username FROM Users WHERE id=:id', array(':id' => 4)));

###Available Functions
Here is an explanation of each of the avilable methods in this abstract class.

---

**init:**

*Parameters:*
None

*Return Values:*
None

Creates a connection to the database based on `$_host`, `$_database`, `$_dbuser`, and `$dbpass`;

---

**executeQuery:**

*Parameters:*
* string `$query` The SQL query
* array `$params` (Optional) Query parameters. Defaults to empty array.
* boolean `$debug` (Optional) Flag to indicate whether to dump the prepared statement. Defaults to false.

*Return Values:*
Returns an array of results or `false` if the query failed to execute.

Executes `$query` bound with the specified `$params` using prepared statements. 

*Example:*

    $dao = new DAO('Users');
    $dao->executeQuery('SELECT username FROM Users WHERE id=:id', array(':id' => 4));
    # Returns:
    # Array
    # (
    # [0] => Array
    #    (
    #        [username] => ynori7
    #    )
    # )

---

**getErrors:**

*Parameters:*
None

*Return Values:*
Returns an array of error information about the last operation performed by this database handle. See [PDO::errorInfo()](http://php.net/manual/en/pdo.errorinfo.php) for more details.