<?php
/*
 * Useful URL functions
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\CoreUtilities\Data;

class Url {
    
    const HYPHEN = '-';

    /**
     * Takes a string and formats it as a URL slug.
     * 
     * @param string $text
     * @return string
     */
    public static function slugify($text) {
        //Note: this is locale-dependent
        $text = iconv("UTF-8", "ASCII//TRANSLIT", $text);
        $text = strtolower($text);

        // replace non letter or non digits with hyphen
        $text = preg_replace('#[^\\pL\d]+#u', self::HYPHEN, $text);
        $text = trim($text, self::HYPHEN);

        if (empty($text)) {
            $text = 'n-a'; //i.e. "Not applicable"
        }

        return $text;
    }
    
    /**
     * Take a slug and make a proper string out of it.
     * Note that this function cannot perfectly reverse slugify()
     * 
     * @param string $slug
     * @return string
     */
    public static function unslugify($slug){
        $text = str_replace(self::HYPHEN, ' ', $slug);
        $text = ucwords($text);
        
        return $text;
    }

}
