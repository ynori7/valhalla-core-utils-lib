Testing Utilities
=================
The Valhalla Core Utils Library expands upon the existing PHP library to include functions to assist with the construction and execution of unit and integration tests.

###Available Functions:
Here is a list of each of the available functions in Collection and how they can be used. Note that each of these functions is static and therefore can be called without instantiating any objects.

---

**publicizeMethod**

*Parameters:*
* string `$className`
* string `$methodName`

*Return Value:*
A `\ReflectionMethod` which can be invoked via `invokeArgs()`

This function accepts a class name and method name and allows you to make the method temporarily public to make the testing of protected and private methods easier.

*Example:* 

    # Taken from one of our unit tests
    $method = TestUtil::publicizeMethod('Valhalla\CoreUtilities\Security\CsrfToken', 'generateToken');
    $token = new CsrfToken();
    $actual = $method->invokeArgs($token, array());