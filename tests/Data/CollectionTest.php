<?php
/* 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\Tests\Data;

use Valhalla\CoreUtilities\Data\Collection;

/**
 * @covers Valhalla\CoreUtilities\Data\Collection
 */
class CollectionTest extends \PHPUnit_Framework_TestCase {
    
    private $noConflictArray1 = array(
            'a' => 'a', 
            'b' => 'b', 
            'c' => 'c',
    );
    private $noConflictArray2 = array(
            'd' => 'd', 
            'e' => array('a', 'b'), 
            'f' => 'f',
    );
    private $conflictArray1 = array(
            'a' => 'a', 
            'b' => 'b', 
            'e' => array('a', 'b'),
    );
    private $conflictArray2 = array(
            'd' => 'd', 
            'e' => 'x', 
            'f' => 'f',
    );
    private $deepNoConflictArray1 = array(
            'a' => 'a', 
            'b' => 'b', 
            'e' => array('c' => 'c'),
    );
    private $deepNoConflictArray2 = array(
            'd' => 'd', 
            'e' => array('a' => 'a', 'b' => 'b'), 
            'f' => 'f',
    );
    private $deepConflictArray1 = array(
            'a' => 'a', 
            'b' => 'b', 
            'e' => array('a' => 'a'),
    );
    private $deepConflictArray2 = array(
            'd' => 'd', 
            'e' => array('a' => 'c', 'b' => 'b'), 
            'f' => 'f',
    );
    
    
    public function testMergeNonRecursiveNoConflicts(){
        $expected = array(
            'a' => 'a',
            'b' => 'b', 
            'c' => 'c',
            'd' => 'd', 
            'e' => array('a', 'b'), 
            'f' => 'f',
        );
        
        $actual = Collection::merge($this->noConflictArray1, $this->noConflictArray2, false);
        
        $this->assertEquals(count($actual), count($expected));
        foreach($actual as $key => $value){
            $this->assertTrue(array_key_exists($key, $expected));
            $this->assertEquals($expected[$key], $value);
        }
    }
    
    public function testMergeNonRecursiveWithConflicts(){
        $expected = array(
            'a' => 'a',
            'b' => 'b', 
            'd' => 'd', 
            'e' => 'x', 
            'f' => 'f',
        );
        
        $actual = Collection::merge($this->conflictArray1, $this->conflictArray2, false);
        
        $this->assertEquals(count($actual), count($expected));
        foreach($actual as $key => $value){
            $this->assertTrue(array_key_exists($key, $expected));
            $this->assertEquals($expected[$key], $value);
        }
    }
    
    public function testMergeRecursiveNoConflicts(){
        $expected = array(
            'a' => 'a', 
            'b' => 'b', 
            'e' => array('c' => 'c', 'a' => 'a', 'b' => 'b'),
            'd' => 'd', 
            'f' => 'f',
        );
        
        $actual = Collection::merge($this->deepNoConflictArray1, $this->deepNoConflictArray2, true);
        
        $this->assertEquals(count($actual), count($expected));
        foreach($actual as $key => $value){
            $this->assertTrue(array_key_exists($key, $expected));
            $this->assertEquals($expected[$key], $value);
        }
    }
    
    public function testMergeRecursiveWithConflicts(){
        $expected = array(
            'a' => 'a', 
            'b' => 'b', 
            'e' => array('a' => 'c', 'b' => 'b'),
            'd' => 'd', 
            'f' => 'f',
        );
        
        $actual = Collection::merge($this->deepConflictArray1, $this->deepConflictArray2, true);
        
        $this->assertEquals(count($actual), count($expected));
        foreach($actual as $key => $value){
            $this->assertTrue(array_key_exists($key, $expected));
            $this->assertEquals($expected[$key], $value);
        }
    }
    
    public function testMergeEmptyArrays(){       
        $actual = Collection::merge(array(), array());
        
        $this->assertEquals(count($actual), 0);       
    }
    
    public function testArrayDiffRecursiveEmpty(){
        $actual = Collection::arrayDiffRecursive(array(), array());
        
        $this->assertEquals(count($actual), 0);
    }
    
    public function testArrayDiffRecursiveNoDiff(){
        $actual = Collection::arrayDiffRecursive($this->noConflictArray1, $this->noConflictArray1);
        
        $this->assertEquals(count($actual), 0);
    }
    
    public function testArrayDiffRecursiveDiff(){
        $test = $this->noConflictArray1;
        $test['d'] = 'x';
        
        $actual = Collection::arrayDiffRecursive($test, $this->noConflictArray1);
        
        $this->assertEquals(count($actual), 1);
        $this->assertTrue(array_key_exists('d', $actual));
    }
    
    public function testArrayDiffRecursiveConflict(){
        $test = $this->noConflictArray1;
        $test['c'] = 'x';
        
        $actual = Collection::arrayDiffRecursive($this->noConflictArray1, $test);
        
        $this->assertEquals(count($actual), 1);
        $this->assertTrue(array_key_exists('c', $actual));
    }
    
    public function testArrayDiffRecursiveDeepDiff(){
        $test = $this->deepConflictArray2;
        $test['e']['a'] = 'x';
        
        $actual = Collection::arrayDiffRecursive($this->deepConflictArray2, $test);
        
        $this->assertEquals(count($actual), 1);
        $this->assertTrue(array_key_exists('e', $actual));
    }
            
}

