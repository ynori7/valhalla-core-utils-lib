HTTP Utilities
==============
The Core Utils Library has a selection of useful utilities to assist with HTTP activities. Currently these modules are divided into two sections: Requests and Sessions. 

The requests section contains everything necessary for handling HTTP requests with and without the use of cURL. This section also contains a request interface which all request modules implement so that your code does not necessarily need to be aware of which request type is being used.

Currently the session section contains an HTTP session object for handling sessions. There also exists a session interface for future session handlers to implement.

Current modules:

* cURL
* BasicRequest
* HttpSession
