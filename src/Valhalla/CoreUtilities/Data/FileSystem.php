<?php
/*
 * File Utilities
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\CoreUtilities\Data;

class FileSystem {
    
    /**
     * Traverses a directory recursively and lists the files / subdirectories
     *
     * @param string $path
     * @param boolean $recursive
     * @param boolean $showDirectories Only really relevant when $recursive is false
     * @return array
     */
    public static function listFiles($path, $recursive = true, $showDirectories = true) {
        $result = array();

        $files = array_diff(scandir($path), array('..', '.'));

        foreach ($files as $file) {
            $isDir = is_dir($path . DIRECTORY_SEPARATOR . $file);
            if ($recursive and $isDir) {
                $result[$file] = self::listFiles($path . DIRECTORY_SEPARATOR . $file);
            }
            else if(!$isDir or $showDirectories){
                $result[] = $file;
            }
        }

        return $result;
    }
}
