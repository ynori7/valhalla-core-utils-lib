cURL
====
The cURL class is a convenient wrapper for PHP's libcurl. This module contains functions to make the execution of GET, POST, PUT, and DELETE requests easy. All requests are configurable with the various cURL options.

###For Example:
    $curl = new cURL();
    $curl->setOptions(
        array('CURLOPT_FAILONERROR' => false)
    );
    $curl->post('http://halls-of-valhalla.org', array('test'=>'blah'));

###Available Functions:
Here is an explanation of each of the available functions for cURL.

---

**__construct: (The Constructor)**

*Parameters:*
None

*Return Values:*
None

Adds the default options to the array of options to be used for this request.

---

**setOptions:**

*Parameters:*
* array `$options`

*Return Values:*
None

Set extra cURL options to use for this instance. The new options are merged on top of the list of default options. See [curl_setopt](http://php.net/manual/en/function.curl-setopt.php) for a list of available options and their possible values.

---

**get:**

*Parameters:*
* string `$url`

*Return Values:*
Returns a string containing the HTTP response.

Executes a GET request on the given URL.

---

**post:**

*Parameters:*
* string `$url`
* array `$payload` (Optional) The data to be posted. Defaults to empty array.

*Return Values:*
Returns a string containing the HTTP response.

Executes a POST request on the given URL with the given POST parameters.

---

**delete:**

*Parameters:*
* string `$url`

*Return Values:*
Returns a string containing the HTTP response.

Executes a DELETE request on the given URL.

---

**put:**

*Parameters:*
* string `$url`
* array `$payload` (Optional) The data to be posted. Defaults to empty array.

*Return Values:*
Returns a string containing the HTTP response.

Executes a PUT request on the given URL with the given PUT parameters.


---

**curl:**

*Parameters:*
* string `$url`
* array `$temporaryOptions` (Optional) Options used for just this request (e.g. postfields). Will be merged with the cURL options array. Defaults to empty array.

*Return Values:*
Returns a string containing the HTTP response. Throws an exception if the request failed.

Executes a request on the given URL. The functions `get()`, `post()`, `put()`, and `delete()` are actually wrappers for this function and the same functionality can be built manually using this function if needed. This function potentially allows more control since you are able to specify any additional cURL options you want for this request (if for some reason you do not want to set these options using `setOptions()`).

###Troubleshooting:
If you see an error message that looks like this:

    "Failed to retrieve data from {url} with error message: {curl_error}"

It means that the request failed. If `CURLOPT_FAILONERROR` is true (which is the default value), then it's possible that the request resulted in a 4xx or 5xx HTTP code. If `CURLOPT_FAILONERROR` is false then it means that you were unable to reach the server (likely due to connect timeout or an invalid URL). More information about the specific error encountered should be displayed in {curl_error} in the error message.
