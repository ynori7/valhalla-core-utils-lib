<?php

/*
 * A set of useful array functions such as recurisively merging two arrays.
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\CoreUtilities\Data;

class Collection {

    /**
     * Merges two arrays. If $recursiveMerge is true then merge the individual items
     * of sub-arrays, otherwise just replace them entirely.
     * 
     * @param array $current
     * @param array $new
     * @param boolean $recursiveMerge
     * @return array
     */
    public static function merge(array $current, array $new, $recursiveMerge = true) {
        foreach ($new as $key => $item) {
            if (array_key_exists($key, $current)) {
                if (is_array($item) and is_array($current[$key]) and $recursiveMerge) {
                    $current[$key] = self::merge($current[$key], $item);
                } else {
                    $current[$key] = $item;
                }
            } else {
                $current[$key] = $item;
            }
        }

        return $current;
    }

    /**
     * Returns an array containing the entries from array1 that are not present 
     * in array2 or are different.
     * 
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public static function arrayDiffRecursive(array $array1, array $array2) {
        $return = array();

        foreach ($array1 as $key => $value) {
            if (array_key_exists($key, $array2)) {
                if (is_array($value)) {
                    $recursiveDiff = self::arrayDiffRecursive($value, $array2[$key]);
                    if (count($recursiveDiff)) {
                        $return[$key] = $recursiveDiff;
                    }
                } else {
                    if ($value != $array2[$key]) {
                        $return[$key] = $value;
                    }
                }
            } else {
                $return[$key] = $value;
            }
        }
        
        return $return;
    }
    
    /**
     * Get a value from the results array at a specific key.
     * If unavailable, return an optionally specified default value.
     * Optionally specify a validation filter.
     *
     * @param array $array
     * @param string|int $key
     * @param string $default
     * @param int $validateFlag
     * @return mixed
     */
    public static function getFromArray(array $array, $key, $default='', $validateFlag = FILTER_DEFAULT){
        return (isset($array[$key]) and self::validateData($array[$key], $default, $validateFlag)) 
                ? $array[$key] : $default;
    }
    
    /**
     * Get data from an array given a path. For example:
     * getFromArrayPath($x, 'data/config/use_cookies')
     * Returns $x['data']['config']['use_cookies']
     * If unavailable, return an optionally specified default value.
     * Optionally specify a validation filter.
     * 
     * @param array $array
     * @param string $path
     * @param string $default
     * @param int $validateFlag
     * @return mixed
     */
    public static function getFromArrayPath(array $array, $path, $default='', $validateFlag = FILTER_DEFAULT) {
        $indices = explode("/", $path);

        foreach ($indices as $key) {
            if (isset($array[$key])) {
                $array = $array[$key];
            } else {
                return $default;
            }
        }

        return self::validateData($array, $default, $validateFlag);
    }
    
    /**
     * Sets a value in an array based on a specified path.
     * 
     * @param array $array
     * @param string $path
     * @param mixed $value
     * @param boolean $createFullPath Whether or not to generate the full path if a key doesn't exist.
     * @throws \Exception
     */
    public static function setArrayPath(array &$array, $path, $value, $createFullPath = false){
        $indices = explode("/", $path);

        foreach ($indices as $k => $key) {
            if (isset($array[$key]) and (is_array($array[$key]) or self::isLast($indices, $k)) ) {
                $array = &$array[$key];
            } else if ($createFullPath) {
                $array[$key] = array(); //set it to an emtpy placeholder for now
                $array = &$array[$key];
            } else {
                throw new \Exception('Invalid path.');
            }
        }
        
        $array = $value;
    }
    
    /**
     * Checks if an element is the last element of an array.
     * 
     * @param array $array
     * @param string|int $key
     * @return boolean
     */
    public static function isLast(&$array, $key) {
        end($array);
        return $key === key($array);
    }
    
    /**
     * Checks if an element is the first element of an array.
     * 
     * @param array $array
     * @param string|int $key
     * @return boolean
     */
    public static function isFirst(&$array, $key) {
        reset($array);
        return $key === key($array);
    }
    
    /**
     * Validates data. If invalid, return an optionally specified default value.
     * Optionally specify a validation filter.
     *
     * @param array $data
     * @param string $default
     * @param int $validateFlag
     * @return mixed
     */
    protected static function validateData($data, $default='', $validateFlag = FILTER_DEFAULT){
        return ((is_array($data) and filter_var_array($data, $validateFlag) !== false) or
            (filter_var($data, $validateFlag) !== false)) ? $data : $default;
    }

}
