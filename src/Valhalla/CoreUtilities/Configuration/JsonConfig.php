<?php
/* 
 * JSON Config Class
 * Loads configuration files in JSON format and marge them together into a single array.
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\CoreUtilities\Configuration;

use Valhalla\CoreUtilities\Data\Collection;

class JsonConfig {
    
    /**
     * @var array
     */
    protected $_config;
    
    /**
     * Gets config from a JSON file and if there is already config loaded, merges it
     * with the existing config. Note that files included later can overwrite values
     * from files included earlier.
     * 
     * @param string $path
     * @param boolean $recursiveMerge
     */
    public function loadFile($path, $recursiveMerge = true){
        $jsonData = $this->getDataFromFile($path);
        
        if(empty($this->_config)){
            $this->_config = $jsonData;
        } else {
            $this->_config = Collection::merge($this->_config, $jsonData, $recursiveMerge);
        }
    }
    
    /**
     * Gets config from a list of JSON files and merges them. Note that the order
     * of the files in the array matters here.
     * 
     * @param array $paths
     * @param boolean $recursiveMerge
     */
    public function loadFiles(array $paths, $recursiveMerge = true){
        foreach($paths as $path){
            $this->loadFile($path, $recursiveMerge);
        }
    }
    
    /**
     * Get a value from the config.
     * 
     * @param string $key
     * @return mixed
     */
    public function get($key){
        $value = false;
        if (isset($this->_config[$key])) {
            $value = $this->_config[$key];
        }
        return $value;
    }
    
    /**
     * Set a value in config. When $mergeRecursively is true, when setting the value,
     * if the current value and the new value are both arrays, they will be merged.
     * 
     * @param string $key
     * @param mixed $value
     * @param boolean $mergeRecursively
     */
    public function set($key, $value, $mergeRecursively = true){
        if(is_array($value) and array_key_exists($key, $this->_config) and $mergeRecursively){
            $this->_config[$key] = Collection::merge($this->_config[$key], $value, $mergeRecursively);
        } else {
            $this->_config[$key] = $value;
        }
    }
    
    /**
     * 
     * @return array
     */
    public function toArray(){
        return $this->_config;
    }
    
    /**
     * Opens gets the contents of the specified JSON file and converts to an array.
     * 
     * @param string $path
     * @return array
     */
    protected function getDataFromFile($path){
        $rawData = file_get_contents($path);
        return json_decode($rawData, true);
    }
    
}

