URL Utilities
=============
The Valhalla Core Utils Library has added a new collection of utility functions to PHP's built-in collection by adding URL utilities. These functions assist with building, parsing, and working with URLs.

###Available Functions:
Here is a list of each of the available functions in Collection and how they can be used. Note that each of these functions is static and therefore can be called without instantiating any objects.

---

**slugify:**

*Parameters:*
* string `$text`

*Return Value:*
A string containing the slugified text.

Accepts a string and and returns a URL slug.
Note: this function is locale-dependent (i.e. when converting non-ASCII characters). Additionally, if `$text` is empty, 'n-a' (i.e. Not applicable) will be returned.

*Example:*

    URL::slugify('Ich bin ein Gummibär!');
    # returns "ich-bin-ein-gummibar"

---

**unslugify:**

*Parameters:*
* string `$slug`

*Return Value:*
A string containing the unslugified text.

Accepts a URL slug and returns a nice title-cased string.
Note: this function does not perfectly reverse the results of `slugify()`.

*For Example:*

    URL::unslugify('ich-bin-ein-gummibar');
    # returns "Ich Bin Ein Gummibar"