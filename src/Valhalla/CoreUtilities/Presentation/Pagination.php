<?php
/*
 * Pagination Class
 * Creates a nice easy-to-use pagination given the current page and the number of items in the database.
 * The number of items per page, pages to display, and SEO pages to display are optional additonal parameters.
 * 
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 * 
 * Example Usage:
 * $page = 3;
 * $totalCount = $dao->countRows();
 * $pagination = new Pagination($page, $totalCount);
 * 
 */

namespace Valhalla\CoreUtilities\Presentation;

class Pagination {

    const DEFAULT_LIMIT = 10;
    const DEFAULT_MAX_PAGES_TO_DISPLAY = 5;
    const DEFAULT_MAX_SEO_PAGES_TO_DISPLAY = 5;

    protected $_currentPage;
    protected $_countPerPage;
    protected $_totalCount;
    
    protected $_maxPagesToDisplay;
    protected $_maxSeoPagesToDisplay;
    protected $_totalPages;
    protected $_currentOffset;
    protected $_pagesToDisplay;
    protected $_seoPagesToDisplay;

    /**
     * 
     * @param int $currentPage 
     * @param int $totalCount The total number of items in the database
     * @param int $limit Number of items to show per page (optional)
     * @param int $maxPagesToDisplay Number of pages to show at a time (optional)
     * @param int $maxSeoPagesToDisplay Number of secondary pages to show at a time (optional)
     */
    public function __construct($currentPage, $totalCount, $limit = self::DEFAULT_LIMIT, 
            $maxPagesToDisplay = self::DEFAULT_MAX_PAGES_TO_DISPLAY, 
            $maxSeoPagesToDisplay = self::DEFAULT_MAX_SEO_PAGES_TO_DISPLAY) {
        $this->_totalCount = $totalCount;
        $this->_currentPage = $currentPage;
        $this->_countPerPage = $limit;
        $this->_maxPagesToDisplay = $maxPagesToDisplay;
        $this->_maxSeoPagesToDisplay = $maxSeoPagesToDisplay;
        $this->init();
    }

    /**
     * Calculate the number of pages and the page numbers to display in the current view.
     */
    protected function init() {
        $this->_totalPages = $this->getTotalPageCount();
        $this->_currentOffset = self::getOffsetFromPage($this->_currentPage, $this->_countPerPage);
        $this->_pagesToDisplay = $this->getPagesToDisplay();
        $this->_seoPagesToDisplay = $this->getSeoPagesToDisplay();
    }

    /**
     * 
     * @return int
     */
    protected function getTotalPageCount() {
        $contentCount = (double) $this->_totalCount;
        $totalPages = (int) ceil($contentCount / $this->_countPerPage);
        return $totalPages;
    }

    /**
     * Get the current offset for the database query given a certain page.
     * 
     * @param int $page
     * @param int $limit
     * @return int
     */
    public static function getOffsetFromPage($page, $limit) {
        $offset = 0;
        if (is_numeric($page) and $page > 0) {
            $offset = ($page - 1) * $limit;
        }
        return $offset;
    }

    /**
     * Summarizes all the data for the pagination in an array.
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'countPerPage' => $this->_countPerPage,
            'currentPage' => $this->_currentPage,
            'totalPages' => $this->_totalPages,
            'currentOffset' => $this->_currentOffset,
            'totalCount' => $this->_totalCount,
            'maxPagesToDisplay' => $this->_maxPagesToDisplay,
            'maxSeoPagesToDisplay' => $this->_maxSeoPagesToDisplay,
            'pagesToDisplay' => $this->_pagesToDisplay,
            'seoPagesToDisplay' => $this->_seoPagesToDisplay,
        );
    }

    /**
     * Gets a list of the page numbers which should be displayed currently.
     * 
     * @return array
     */
    protected function getPagesToDisplay() {
        $lowerHalf = (int) floor($this->_maxPagesToDisplay / 2.0);
        $startPage = 1; //default
        if ($this->_currentPage <= $this->_totalPages and ( $this->_currentPage - $lowerHalf) > 1) {
            $startPage = $this->_currentPage - $lowerHalf;
        }
        $endPage = $startPage + ($this->_maxPagesToDisplay - 1);
        if ($endPage > $this->_totalPages) {
            $endPage = ($this->_totalPages > 0) ? $this->_totalPages : 1; //so we dont show non-existing pages
        }
        $pages = range($startPage, $endPage);
        
        return $pages;
    }

    /**
     * Gets a list of page numbers for the secondary paging list (for SEO purposes typically).
     * 
     * @return array
     */
    protected function getSeoPagesToDisplay() {
        $seoPages = array();

        $currentSeoPage = ($this->_currentPage * 10) + 1;

        while ($currentSeoPage <= $this->_totalPages and count($seoPages) < $this->_maxSeoPagesToDisplay) {
            $seoPages[] = $currentSeoPage;
            $currentSeoPage++;
        }

        return $seoPages;
    }

}
