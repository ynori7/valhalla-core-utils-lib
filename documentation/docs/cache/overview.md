Cache
=====
Here you can find information about the Valhalla Core Utils Library's cache classes. The cache classes provide useful wrappers for popular caching services and techniques and additionally useful functions for making cache management easy.

Currently we have caching modules for the following cache methods:

* Memcached