Valhalla Core Utilities
========================

This is a project intended to supply useful utility classes to ensure that you do not need to reinvent the wheel in each of your projects. 


Usage:
------
**With Composer:**

The best way to use this project is to install it via Composer. To do so, add the following to your composer.json:

    "require": {
        "valhalla/valhalla-core-utils-lib": "dev-master"
    }

Now you can just do:

    use Valhalla\CoreUtilities\Http\cURL;
    
    $curl = new cURL();
    $curl->setOptions(array('CURLOPT_FAILONERROR' => false));
    print_r($curl->post('http://halls-of-valhalla.org', array('test'=>'blah')));

If you are not using composer with your project already, you can place the above composer.json code in your project and then execute the following commands from the root of this project:

    curl -sS https://getcomposer.org/installer | php
    php composer.phar install

Now you can include the composer autoloader in your files:

    include_once('valhalla-core-utils-lib/vendor/autoload.php');


Current modules:
----------------

* Memcached
* cURL
* Basic HTTP Requests (for non-cURL requests)
* Array Utilities
* String Utilities
* URL Utilities
* File System Utilities
* Testing Utilities
* Cross-site request forgery tokens
* Session
* PDO SQL database connection
* Pagination
* JSON Config Handler