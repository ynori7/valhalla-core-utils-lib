<?php
/* 
 * cURL Class
 * Wrapper for the PHP cURL library
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 * 
 * Example Usage:
 * use Valhalla\CoreUtilities\Http\Request\cURL;
 * $curl = new cURL();
 * $curl->setOptions(array('CURLOPT_FAILONERROR' => false));
 * print_r($curl->post('http://halls-of-valhalla.org', array('test'=>'blah')));
 */

namespace Valhalla\CoreUtilities\Http\Request;

class cURL implements RequestInterface {
    private $_options = array();
    private $_defaultCurlOptions = array(
        'CURLOPT_FOLLOWLOCATION' => 1,
        'CURLOPT_FAILONERROR' => 1,
        'CURLOPT_RETURNTRANSFER' => 1,
        'CURLOPT_TIMEOUT' => 30,
        'CURLOPT_SSL_VERIFYPEER' => false,
    );
    
    public function __construct() {
        $this->_options = $this->_defaultCurlOptions;
    }
    
    /**
     * Set extra cURL options to use for this instance.
     * @param array $options
     */
    public function setOptions(array $options){
        $this->_options = array_merge($this->_defaultCurlOptions, $options);
    }
   
    /**
     * 
     * @param string $url
     * @return string
     */
    public function get($url){
        return $this->curl($url);
    }
    
    /**
     * 
     * @param string $url
     * @param array $payload
     * @return string
     */
    public function post($url, array $payload = array()){
        $postOptions = array(
            'CURLOPT_POST' => count($payload),
            'CURLOPT_POSTFIELDS' => http_build_query($payload),
        );
        
        return $this->curl($url, $postOptions);
    }
    
    /**
     * 
     * @param string $url
     * @return string
     */
    public function delete($url){
        $deleteOptions = array(
            'CURLOPT_CUSTOMREQUEST' => 'DELETE',
        );
        
        return $this->curl($url, $deleteOptions);
    }
    
    /**
     * 
     * @param string $url
     * @param array $payload
     * @return string
     */
    public function put($url, array $payload = array()){
        $putOptions = array(
            'CURLOPT_CUSTOMREQUEST' => 'PUT',
            'CURLOPT_POSTFIELDS' => http_build_query($payload),
        );
        
        return $this->curl($url, $putOptions);
    }
    
    /**
     * Wrapper for php curl. Allows specifying additional options
     *
     * @param string $url
     * @param array $temporaryOptions Options used for just this request (like postfields)
     * @return string $response
     * @throws \Exception
     */
    public function curl($url, array $temporaryOptions = array()) {
        $options = array_merge($this->_options, $temporaryOptions);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        foreach ($options as $opt_key => $opt_value) {
            curl_setopt($ch, constant(strtoupper($opt_key)), $opt_value);
        }
        $response = curl_exec($ch);
        
        if ($response === false) {
          throw new \Exception("Failed to retrieve data from $url with error message: " . curl_error($ch));
        }

        curl_close($ch);

        return $response;
    }
}


