<?php

/* 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\Tests\Security;

use Valhalla\CoreUtilities\Security\CsrfToken;
use Valhalla\CoreUtilities\Testing\TestUtil;

/**
 * @covers Valhalla\CoreUtilities\Security\CsrfToken
 */
class CsrfTokenTest extends \PHPUnit_Framework_TestCase {
    
    public function testIsTokenSet_Set(){
        $data = array(CsrfToken::TOKEN_NAME => 'blah');
        $token = new CsrfToken();
        $actual = $token->isTokenSet($data);
        
        $this->assertTrue($actual);
    }
    
    public function testIsTokenSet_NotSet(){
        $data = array('bloop' => 'blah');
        $token = new CsrfToken();
        $actual = $token->isTokenSet($data);
        
        $this->assertFalse($actual);
    }
    
    /*************************/
    
    public function testGenerateToken(){
        $method = TestUtil::publicizeMethod('Valhalla\CoreUtilities\Security\CsrfToken', 'generateToken');
        
        $token = new CsrfToken();
        $actual = $method->invokeArgs($token, array());
        
        $this->assertEquals(strlen($actual), 36);
    }
    
}