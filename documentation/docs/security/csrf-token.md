CSRF Token
==========
The CSRF Token module is an easy-to-use tool to prevent cross-site request forgery vulnerabilities. This module implements the Synchronizer Token Pattern. For more information about CSRF and the Synchronizer Token Pattern, see [CSRF: Demystified](http://halls-of-valhalla.org/beta/articles/cross-site-request-forgery-demystified,47/). 

###Example Usage:
    $csrfToken = new CsrfToken();
    if($csrfToken->isValidToken($_POST)){
    //CSRF token is valid, so perform action
    }
    
To display in forms, just use:
    
    $token = $csrfToken->getToken();

Note that the CsrfToken class depends on Valhalla's Session objects (i.e. anything implementing SessionInterface)

###Available Functions:
Here is an explanation of the available methods for the CsrfToken class:

**getToken:**

*Parameters:*
None

*Return Values:*
Returns a CSRF token.

Gets a token from the session. If there isn't one, puts one there.

---

**isTokenSet:**

*Parameters:*
* array `$requestParams`

*Return Values:*
Returns true if the token is set in the request parameters, otherwise false.

---

**isValidToken:**

*Parameters:*
* array `$requestParams`

*Return Values:*
Returns true if the token is set and is valid (i.e. matches the token in the session), otherwise return false.

*Example:*

    $csrfToken->isValidToken($_GET)

---

**setSession:**

*Parameters:*
* Valhalla\CoreUtilities\Http\Session\SessionInterface `$session`

*Return Values:*
None

Sets the session object which will be used to store the token.

---

**getSession:**

*Parameters:*
None

*Return Values:*
Returns the SessionInterface object which should be used to store the CSRF token. If there was none set, an instance of HttpSession will be returned.