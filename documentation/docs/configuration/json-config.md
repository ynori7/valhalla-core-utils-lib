JSON Config
===========
The JSON Config class allows you to load configuration files in JSON format and marge them together into a single object.

###Available Functions:
Here is an explanation of each of the available functions for JsonConfig.

---

**loadFile:**

*Parameters:*
* string `$path`
* boolean `$recursiveMerge` (Optional) Whether configs should be recursively merged. Defaults to true.

*Return Value:*
None

Gets config from a JSON file and if there is already config loaded, merges it with the existing config. Note that files included later can overwrite values from files included earlier.

*Example Usage:*

    $config = new JsonConfig();
    $config->loadFile('config/main.config');
    $config->loadFile('config/local.config');

----
    
**loadFiles:**

*Parameters:*
* array `$paths`
* boolean `$recursiveMerge` (Optional) Whether configs should be recursively merged. Defaults to true.

*Return Value:*
None

Like `loadFile()`, but instead of accepting a single path, accepts an array of paths. Note that the order in the array matters.

*Example Usage:*

    $config = new JsonConfig();
    $config->loadFiles(array('config/main.config', 'config/local.config'));

---

**get:**

*Parameters:*
* string `$key`

*Return Value:*
Returns a value from the config given a specified key.

---

**set:**

*Parameters:*
* string `$key`
* mixed `$value`
* boolean `$recursiveMerge` (Optional) Whether the value should be recursively merged. Defaults to true.

*Return Value:*
None

Set a value in config. When `$mergeRecursively` is true, when setting the value, if the current value and the new value are both arrays, they will be merged.

---

**toArray:**

*Parameters:*
None

*Return Value:*
Returns the merged configs as a PHP array.