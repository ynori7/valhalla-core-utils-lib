Memcached
=========

Valhalla's Memcached class is easy-to-use Memcached wrapper with advanced logic to ensure valid data is always returned by utilizing logic so that if it is not possible to retrieve new data, the previously cached result will be returned. 

The Memcached class's constructor requires an array containing server and caching configuration in the following format:

    array(
        'servers' => array(
            array('localhost', 11211),
        ),
        'default_ttl' => 600,
        'max_age' => 604800,
    )
    
- **servers**: This should be a list of arrays containing hostname and port. You can specify as many servers as you want.
- **default_ttl**:  The default time which cache entries should remain valid. When the cached entry is invalid, new data should be retrieved. If it is not possible to retrieve new data, the previously cached result will be returned.
- **max_age**: The point in time at which the cached entry will be removed from Memcached regardless of whether or not it's possible to retrieve new data.

When using the Memcached class, you can manually fetch results from cache with the `fetch()` method, or you can used the advanced fetching logic described above with the `getDataWithCaching()` method. This method will return the previously cached result for a time when unable to successfully update the cache.

###Example Usage:
    use Valhalla\CoreUtilities\Cache\Memcached;
    Class Test{
      public function getData(array $parameters = array()){
        //Here one would normally retreive data from a database or API, but for simplicity's sake we'll return static data
        return array(
            'a' => 'blah',
            'b' => 'werwer',
            'c' => 'blorp',
        );
      }
      public function testCall(){
        $serverConfig = array(
            'servers' => array(
                array('localhost', 11211),
            ),
            'default_ttl' => 600,
            'max_age' => 604800,
        );
        $c = new Memcached($serverConfig);
        
        return $c->getDataWithCaching($this, 'getData');
      }
    }


###Available Functions:
Here is an explanation of each of the available functions for Memcached.

---

**__construct: (The Constructor)**

*Parameters:*
* array `$cacheConfig` The configuration for the cache and Memcached servers (see above)

*Return Values:*
None

Creats an instance of the Memcached object and sets the server config.

**createCacheKey:**

*Parameters:*
* string `$classname`
* array `$parameters` (Optional) Defaults to empty array.

*Return Value:*
Returns a unique id for inserting into cache based on the classname and parameters.

*Example:*

    $cacheKey = $cache->createCacheKey(__CLASS__, array(__FUNCTION__, $id));

---

**contains:**

*Parameters:*
* string `$id` A cache Id

*Return Value:*
Returns `true` if an entry exists in cache with the given id, otherwise `false`.

---

**replace:**

*Parameters:*
* string `$id` A cache Id
* mixed `$payload` The data to save in cache
* int `$lifetime` (Optional) Defaults to 0 which means to use the default TTL from the server settings

*Return Value:*
Returns `true` on success, otherwise `false`.

Given a cache id, payload (i.e. The data to save in cache), and an optional time-to-live value, update an existing entry in cache.

---

**save:**

*Parameters:*
* string `$id` A cache Id
* mixed `$payload` The data to save in cache
* int `$lifetime` (Optional) Defaults to 0 which means to use the default TTL from the server settings

*Return Value:*
Returns `true` on success, otherwise `false`.

Given a cache id, payload (i.e. The data to save in cache), and an optional time-to-live value, inserts an existing entry into cache.

---

**fetch:**

*Parameters:*
* string `$id` A cache Id

*Return Value:*
Returns an entry from cache with the given cache id. Returns `false` if entry does not exist.

---

**isStillValid:**

*Parameters:*
* array `$cachedData` An entry from cache

*Return Value:*
Given a result from cache (e.g. from `fetch()`), returns `true` or `false` to indicate whether the cached result is still within it's allowed time-to-live.

---

**getDataWithCaching:**

*Parameters:*
* object `$class` The class which is requesting data
* callable `$function` The function defined in $class for retreiving the desired data
* array `$parameters` (Optional) Arguments to `$class->{$function}()`. Defaults to empty array.
* int `$ttl` The user-defined lifetime for the cache

*Return Value:*
Returns the cached data.

Given a class name, function name, an optional array of parameters, and an optional time-to-live, retrieve an entry from cache. If the entry is no longer valid, attempt to retrieve new data by calling `$class->{$function}($parameters)`. If unable to retrieve new data, continues to serve the old, invalid cached result until that result has reached max_age at which point it will be deleted. To see an example, see the example above under "Example Usage".