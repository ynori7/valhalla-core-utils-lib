Testing Utilities
==============
In this section you can find a collection of useful utilities for unit and integration testing. These each include many useful functions which build upon, add to, and extend existing PHP functions.

Included are:

* TestUtil which contains a collection of useful functions for assisting with the building and execution of tests