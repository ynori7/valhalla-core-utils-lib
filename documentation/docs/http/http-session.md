HTTP Session
============
The HTTP Session class is a convenient wrapper around PHP's built-in session handling. This module provides a useful interface for getting and setting data in the session plus additional features not readily available with PHP's native session handling.

###Example Usage:
Valhalla's session handler is quite easy to use. The class is a convenient singleton which handles everything for you.

    $session = HttpSession::getInstance();
    $session->get('username');

You can even adjust the default PHP session handling method:

    $mySession = new \MySession();
    session_set_save_handler(
        array(&$mySession, 'open'),
        array(&$mySession, 'close'),
        array(&$mySession, 'read'),
        array(&$mySession, 'write'),
        array(&$mySession, 'destroy'),
        array(&$mySession, 'gc')
    );
    $session = HttpSession::getInstance(); //this will now use the user-defined session handler
    $session->get('username');

###Available Functions:
Here is a list of available functions for the HTTP session class and how they work:

**startSession:**

*Parameters:*
None

*Return Values:*
Returns a boolean indicating whether the session has been successfully started.

Starts the session (or restarts) and returns the session's status.

---

**get:**

*Parameters:*
* string `$attribute`

*Return Values:*
Returns a string containing the requested data from the session, or `false` if not found.

Retrieves a particular attribute from the session.

---

**set:**

*Parameters:*
* string `$attribute`
* string `$value`

*Return Values:*
None

Set an attribute in the session.

---

**getFlashMessage:**

*Parameters:*
None

*Return Values:*
Returns a string containing the flash message or `false` if there was none.

Gets flash messages, i.e. messages that should be consumed only once and then are removed automatically.

---

**setFlashMessage:**

*Parameters:*
* string `$message`

*Return Values:*
None

Sets the flash message.

---

**regenerateId:**

*Parameters:*
None

*Return Values:*
None

Resets the session id. This function should be called when a user is logged in to prevent session fixation.

---

**destroySession:**

*Parameters:*
None

*Return Values:*
None

Destroy the current session.

---

**getSession:**

*Parameters:*
None

*Return Values:*
Returns an array containing the current `$_SESSION` data.


---

**setSession:**

*Parameters:*
* array `$session`

*Return Values:*
None

Sets the `$_SESSION` based on the parameters.

---

**getInstance:**

*Parameters:*
None

*Return Values:*
Returns the existing instance of HttpSession or creates one and then returns it.