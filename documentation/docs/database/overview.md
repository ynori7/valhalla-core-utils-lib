Database Utilities
==================
The Valhalla Core Utilities Library provides a collection of useful database modules which extend the functionality of the built-in PHP libraries. Currently we have extensions for PDO MySQL, but it is planned in the future to include modules for MongoDB, Neo4j, and more.

Currently we have the following config modules:

* PDO MySQL Wrapper