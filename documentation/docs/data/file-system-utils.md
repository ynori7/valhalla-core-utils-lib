File System Utilities
=====================
Valhalla's file system utilities class adds useful functions for working with files and directories and for traversing the directory tree.

###Available Functions
Here is a list of each of the available functions in FileSystem and how they can be used. Note that each of these functions is static and therefore can be called without instantiating any objects.

---

**listFiles:**

*Parameters:*
* string `$path`
* boolean `$recursive` (Optional) Defaults to true.
* boolean `$showDirectories` (Optional) Defaults to true.

*Return Value:*
An array of files and directories.

The function traverses a directory indicated by `$path` and lists the files. If `$recursive` is true, the function traverses the directory and sub-directories recursively.

`$showDirectories` is only really relevant when `$recursive` is false. In this case, directories will be listed along with files in the resulting array, otherwise only files will be shown.