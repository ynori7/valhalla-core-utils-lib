<?php

/* 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\Tests\Http\Request;

use Valhalla\CoreUtilities\Http\Request\BasicRequest;

/**
 * @covers Valhalla\CoreUtilities\Http\Request\BasicRequest
 */
class BasicRequestTest extends \PHPUnit_Framework_TestCase {
    
    public function testGet_GoodUrl(){
        $req = new BasicRequest();
        $response = $req->get('http://www.google.com');
        
        $this->assertNotEmpty($response);
    }
    
    /**
     * @expectedException \Exception
     */
    public function testGet_BadUrl(){
        $req = new BasicRequest();
        $req->get('http://www.google.potato');
    }
    
}