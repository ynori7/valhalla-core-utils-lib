<?php
/* 
 * Interface for HTTP Requests
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 */

namespace Valhalla\CoreUtilities\Http\Request;

interface RequestInterface {
  
    public function setOptions(array $options);  
    public function get($url);   
    public function post($url, array $payload = array()); 
    public function delete($url);
    public function put($url, array $payload = array());
    
}