Pagination
==========


The pagination class make pagination-generation easy. By simply instantiating the object and passing in the current page and the total count from your database, you instantly have everything you need to make the perfect pagination including a list of pages to display and a secondary page list for SEO!

###Example Usage:

Simply instantiate a Pagination object and indicate the current page and total result count.

    $page = $_GET['page']; //page 3
    $totalCount = $db->countRows(); //500 items
    $pagination = new Pagination($page, $totalCount);
    $pageData = $pagination->toArray();

Now `$pageData` will contain all the information you need to make your paging. Here's what the above example produces:

    Array
    (
        [countPerPage] => 10
        [currentPage] => 3
        [totalPages] => 50
        [currentOffset] => 20
        [totalCount] => 500
        [maxPagesToDisplay] => 5
        [maxSeoPagesToDisplay] => 5
        [pagesToDisplay] => Array
            (
                [0] => 1
                [1] => 2
                [2] => 3
                [3] => 4
                [4] => 5
            )
    
        [seoPagesToDisplay] => Array
            (
                [0] => 31
                [1] => 32
                [2] => 33
                [3] => 34
                [4] => 35
            )
    
    )

The Pagination class is of course quite configurable. You can optionally specify parameters when instantiating the object to control the count per page, number of page to display, and number of SEO pages to display.

###Available Functions:
Here is a list of each of the available functions in Pagination and how they work.

---

**__construct: (The Constructor)**

*Parameters:*
*int $currentPage 
* int `$totalCount` The total number of items in the database
* int `$limit` (Optional) Number of items to show per page. Defaults to 10
* int `$maxPagesToDisplay` (Optional) Number of pages to show at a time. Defaults to 5
* int `$maxSeoPagesToDisplay` (Optional) Number of secondary pages to show at a time. Defaults to 5

*Return Values:*
None

Instantiates an instance of the Pagination object and prepares all of the necessary data basd on the parameters.

---

**getOffsetFromPage:**

*Parameters:*
* int `$page`
* int `$limit`

*Return Value:*
Returns an integer showing the offset based on the page and limit.

Note that this function is static and can be used without instantiating a Pagination object due to the potential usefulness of the function.

---

**toArray:**

*Parameters:*
None

*Return Value:*
Returns an array summarizing all the data for the pagination.