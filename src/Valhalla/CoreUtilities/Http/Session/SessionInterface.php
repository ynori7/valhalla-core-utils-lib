<?php
/*
 * Interface for Session Classes
 * Defines functions which should be available for all session classes
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 * 
 */

namespace Valhalla\CoreUtilities\Http\Session;

interface SessionInterface {
        
    public function get($attribute);
    public function set($attribute, $value);
    public function getFlashMessage();
    public function setFlashMessage($message);
    public function regenerateId();
    public function destroySession();
    public function getSession();
    public function setSession($session);
    
}
