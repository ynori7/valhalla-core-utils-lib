<?php
/*
 * Session Class
 * Wrapper for basic HTTP sessions.
 * 
 * @author ynori7
 * @copyright Copyright (c) 2014, halls-of-valhalla.org
 * @license http://creativecommons.org/licenses/by-sa/4.0/ Creative Commons Attribution-ShareAlike 4.0 International License. 
 * 
 * Example Usage:
 * $session = HttpSession::getInstance();
 * $session->get('username'); 
 */

namespace Valhalla\CoreUtilities\Http\Session;

class HttpSession implements SessionInterface {
    const SERVER_PREFIX = 'ValhallaCoreUtilities'; //Prefix for all class-specific attributes.
    
    const SESSION_STARTED = true;
    const SESSION_NOT_STARTED = false;
   
    /**
     * The status of the session (started / not started)
     * @var boolean
     */
    private $sessionState = self::SESSION_NOT_STARTED;
    /**
     * @var HttpSession
     */
    private static $instance;
       
    /**
     * Starts the session (or restarts) and returns the session's status.
     * 
     * @return boolean
     */
    public function startSession() {
        if ( $this->sessionState == self::SESSION_NOT_STARTED ) {
            $this->sessionState = session_start();
        }
       
        return $this->sessionState;
    }

    /**
     * Retrieve an attribute from the session data.
     * 
     * @param string $attribute
     * @return string|boolean
     */
    public function get($attribute) {
        $value = false;
        $session = $this->getSession();
        if (isset($session[$attribute])) {
            $value = $session[$attribute];
        }
        return $value;
    }

    /**
     * Set an attribute in the session.
     * 
     * @param string $attribute
     * @param string $value
     */
    public function set($attribute, $value) {
        $_SESSION[$attribute] = $value;
    }

    /**
     * Gets flash messages, i.e. messages that should be consumed only once and then removed automatically.
     * 
     * @return string|boolean
     */
    public function getFlashMessage() {
        $message = $this->get(self::SERVER_PREFIX . 'flash');
        if ($message !== false) { //remove the message once it's consumed
            unset($_SESSION[self::SERVER_PREFIX . 'flash']);
        }
        return $message;
    }

    /**
     * Sets the flash message.
     * 
     * @param string $message
     */
    public function setFlashMessage($message) {
        $this->set(self::SERVER_PREFIX . 'flash', $message);
    }

    /**
     * Resets the session id. This function should be called when a user is logged in.
     */
    public function regenerateId() {
        session_regenerate_id();
    }

    /**
     * Destroy the current session
     */
    public function destroySession() {
        session_destroy();
    }

    /**
     * 
     * @return array
     */
    public function getSession() {
        return $_SESSION;
    }

    /**
     * 
     * @param array $session
     */
    public function setSession($session) {
        $_SESSION = $session;
    }
    
    private function __construct() { }
   
    /**
     * Get an instance of the session object and begin the session if not already started.
     * 
     * @return HttpSession
     */
    public static function getInstance() {
        if ( !isset(self::$instance)) {
            self::$instance = new self;
        }
       
        self::$instance->startSession();
       
        return self::$instance;
    }
}
